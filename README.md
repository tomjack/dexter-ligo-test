This is WIP on testing
[dexter.mligo](https://gitlab.com/dexter2tz/dexter2tz/) with LIGO.

It currently requires LIGO built from source at rev
[51d13c6bd4](https://gitlab.com/ligolang/ligo/-/commit/51d13c6bd446eb3856b218bb319ae12f8f886978?merge_request_iid=1169).

To run:

```
git submodule update --init
ligo test dexter.test.mligo test
```

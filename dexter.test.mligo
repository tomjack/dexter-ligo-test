#include "./dexter2tz/lqt_fa12.mligo"
let lqt_main = main
type lqt_storage = storage
type lqt_param = parameter
#include "./dexter2tz/dexter.mligo"
let dex_main = main
type dex_storage = storage
type dex_param = entrypoint


let past = ("2009-09-09T09:09:09Z" : timestamp)
let now = ("2010-10-10T10:10:10Z" : timestamp)
let future = ("2011-11-11T11:11:11Z" : timestamp)

let src () = Test.nth_bootstrap_account 1

type deploy_params =
  { tok : nat ;
    lqt : nat ;
    xtz : tez ;
    lqt_provider : address ;
    provider_tok : nat ;
    dex_manager : address }

type deploy_result =
  { dex_addr : (dex_param, dex_storage) typed_address ;
    lqt_addr : (lqt_param, lqt_storage) typed_address ;
    tok_addr : (lqt_param, lqt_storage) typed_address }

let approve_allowance (deployed : deploy_result) (allowance : nat) =
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "approve" deployed.tok_addr : approve contract)
    { spender = Tezos.address (Test.to_contract deployed.dex_addr) ;
      value = allowance }
    0tz

let deploy (params : deploy_params) =
  (* originate tok (reusing lqt_fa12 here) *)
  let tok_storage =
    { tokens = (Big_map.literal [(params.lqt_provider, params.provider_tok + params.tok)] : tokens) ;
      allowances = (Big_map.empty : allowances) ;
      (* src as admin so we can use mintOrBurn in
         test_update_token_pool *)
      admin = src () ;
      total_supply = params.provider_tok + params.tok } in
  let (tok_addr, _, _) = Test.originate lqt_main tok_storage 0tz in

  (* originate dex *)
  let dex_storage =
    build_storage
      {lqtTotal = params.lqt ;
       manager = params.dex_manager ;
       tokenAddress = Tezos.address (Test.to_contract tok_addr)} in
  let (dex_addr, _, _) = Test.originate dex_main dex_storage params.xtz in

  (* originate lqt *)
  let lqt_storage =
    { tokens = (Big_map.literal [(params.lqt_provider, params.lqt)] : tokens) ;
      allowances = (Big_map.empty : allowances) ;
      admin = Tezos.address (Test.to_contract dex_addr) ;
      total_supply = params.lqt } in
  let (lqt_addr, _, _) = Test.originate lqt_main lqt_storage 0tz in

  (* provide liquidity *)
  let () =
    Test.transfer_to_contract_exn
      (Test.to_contract dex_addr : unit contract)
      ()
      params.xtz in
  let () =
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "transfer" tok_addr : transfer contract)
      { address_from = src () ;
        address_to = Tezos.address (Test.to_contract dex_addr) ;
        value = params.tok }
      0tz in
  let () =
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "updateTokenPool" dex_addr : unit contract)
      ()
      0tz in

  (* set lqt address in dex *)
  let () =
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "setLqtAddress" dex_addr : address contract)
      (Tezos.address (Test.to_contract lqt_addr))
      0tz in

  { dex_addr = dex_addr ;
    lqt_addr = lqt_addr ;
    tok_addr = tok_addr }

let clean () =
  begin
    Test.reset_state 2n [10000000000n; 10000000000n];
    Test.set_now now
  end

let basic_setup () =
  let () = clean () in
  let () = Test.set_source (src ()) in
  let deployed =
    deploy { lqt = 1000000n ;
             tok = 1000000n ;
             xtz = 1tz ;
             lqt_provider = src () ;
             provider_tok = 1000000000000n ;
             dex_manager = src () } in
  let () = approve_allowance deployed 1000000000000n in
  deployed


(* dex entrypoint helpers *)

let do_add_liquidity
    (deployed : deploy_result)
    (add_liquidity : add_liquidity)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract
    (Test.to_entrypoint "addLiquidity" dex_addr : add_liquidity contract)
    add_liquidity
    amt

let assert_add_liquidity
    (deployed : deploy_result)
    (add_liquidity : add_liquidity)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "addLiquidity" dex_addr : add_liquidity contract)
    add_liquidity
    amt

let assert_remove_liquidity
    (deployed : deploy_result)
    (remove_liquidity : remove_liquidity) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "removeLiquidity" dex_addr : remove_liquidity contract)
    remove_liquidity
    0tz

let assert_xtz_to_token
    (deployed : deploy_result)
    (xtz_to_token : xtz_to_token)
    (amt : tez) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "xtzToToken" dex_addr : xtz_to_token contract)
    xtz_to_token
    amt

let assert_token_to_xtz
    (deployed : deploy_result)
    (token_to_xtz : token_to_xtz) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "tokenToXtz" dex_addr : token_to_xtz contract)
    token_to_xtz
    0tz

let assert_set_baker
    (deployed : deploy_result)
    (set_baker : set_baker) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "setBaker" dex_addr : set_baker contract)
    set_baker
    0tz

let assert_set_manager
    (deployed : deploy_result)
    (manager : address) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "setManager" dex_addr : address contract)
    manager
    0tz

let assert_update_token_pool
    (deployed : deploy_result) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "updateTokenPool" dex_addr : unit contract)
    ()
    0tz

let assert_token_to_token
    (deployed : deploy_result)
    (token_to_token : token_to_token) =
  let { dex_addr } = deployed in
  Test.transfer_to_contract_exn
    (Test.to_entrypoint "tokenToToken" dex_addr : token_to_token contract)
    token_to_token
    0tz


(* assertion helpers *)

(* unused, because we get no information about the error. we use
   transfer_to_contract_exn instead *)
(* let assert_success (result : test_exec_result) : unit =
 *   match result with
 *   | Success -> ()
 *   | Fail _ -> failwith "expected success, got error" *)

let assert_error (error_code : nat) (result : test_exec_result) : unit =
  match result with
  | Success -> failwith "expected error, got success"
  | Fail e ->
    (match e with
     | Rejected (err, _) ->
       let error_code = Test.compile_value error_code in
       if Test.michelson_equal err error_code
       then ()
       else failwith "wrong error" (* sad *)
     | Other ->
       failwith "other error" (* sad *))

type pools =
  { xtz_pool : tez ;
    token_pool : nat ;
    lqt_total : nat }

let assert_dex_pools (deployed : deploy_result) (pools : pools) : unit =
  let { dex_addr } = deployed in
  let storage = Test.get_storage dex_addr in
  begin
    if storage.xtzPool = pools.xtz_pool then () else failwith "incorrect xtzPool";
    if storage.tokenPool = pools.token_pool then () else failwith storage.tokenPool;
    if storage.lqtTotal = pools.lqt_total then () else failwith storage.lqtTotal
  end

let assert_tokens
    (tok_addr : (lqt_param, lqt_storage) typed_address)
    (addr : address) (tok : nat) : unit =
  let storage = Test.get_storage tok_addr in
  let actual_tok = Big_map.find_opt addr storage.tokens in
  let actual_tok =
    match actual_tok with
    | None -> 0n
    | Some actual_tok -> actual_tok in
  if tok = actual_tok
  then ()
  else failwith actual_tok

let assert_tokens
    (tok_addr : (lqt_param, lqt_storage) typed_address)
    (addr : address) (tok : nat) : unit =
  let storage = Test.get_storage tok_addr in
  let actual_tok = Big_map.find_opt addr storage.tokens in
  let actual_tok =
    match actual_tok with
    | None -> 0n
    | Some actual_tok -> actual_tok in
  if tok = actual_tok
  then ()
  else failwith actual_tok

let assert_tok
    (deployed : deploy_result)
    (addr : address) (tok : nat) : unit =
  let { tok_addr } = deployed in
  assert_tokens tok_addr addr tok

let assert_lqt
    (deployed : deploy_result)
    (addr : address) (lqt : nat) : unit =
  let { lqt_addr } = deployed in
  assert_tokens lqt_addr addr lqt


(* tests *)

let test_setup () =
  let deployed = basic_setup () in
  begin
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 1000000n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 1000000000000n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test_add_liquidity () =
  let deployed = basic_setup () in
  begin
    assert_add_liquidity
      deployed
      { owner = src () ;
        minLqtMinted = 1000000n ;
        maxTokensDeposited = 1000000n ;
        deadline = future }
      1tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 2tz ;
        token_pool = 2000000n ;
        lqt_total = 2000000n } ;
    assert_tok deployed (src ()) 999999000000n ;
    assert_lqt deployed (src ()) 2000000n
  end

let test_add_more_liquidity () =
  let deployed = basic_setup () in
  begin
    assert_add_liquidity
      deployed
      { owner = src () ;
        minLqtMinted = 2000000n ;
        maxTokensDeposited = 2000000n ;
        deadline = future }
      2tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 3tz ;
        token_pool = 3000000n ;
        lqt_total = 3000000n } ;
    assert_tok deployed (src ()) 999998000000n ;
    assert_lqt deployed (src ()) 3000000n
  end

let test_remove_liquidity () =
  let deployed = basic_setup () in
  begin
    assert_remove_liquidity
      deployed
      { to_ = src () ;
        lqtBurned = 500000n ;
        minXtzWithdrawn = 500000mutez ;
        minTokensWithdrawn = 500000n ;
        deadline = future } ;
    assert_dex_pools
      deployed
      { xtz_pool = 500000mutez ;
        token_pool = 500000n ;
        lqt_total = 500000n } ;
    assert_tok deployed (src ()) 1000000500000n ;
    assert_lqt deployed (src ()) 500000n
  end

let test_xtz_to_token () =
  let deployed = basic_setup () in
  begin
    assert_xtz_to_token
      deployed
      { to_ = src () ;
        minTokensBought = 499248n ;
        deadline = future }
      1tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 2tz ;
        token_pool = 500752n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 1000000499248n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test_token_to_xtz () =
  let deployed = basic_setup () in
  begin
    assert_token_to_xtz
      deployed
      { to_ = src () ;
        tokensSold = 500000n ;
        minXtzBought = 332665mutez ;
        deadline = future } ;
    assert_dex_pools
      deployed
      { xtz_pool = 667335mutez ;
        token_pool = 1500000n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 999999500000n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test_set_baker () =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_set_baker
      deployed
      (* TODO put key_hash here when possible *)
      { baker = (None : key_hash option) ;
        freezeBaker = true } ;
    let storage = Test.get_storage dex_addr in
    (* TODO test that delegate was actually set, when possible *)
    if storage.freezeBaker then () else failwith "expected freezeBaker = true"
  end

let test_set_manager () =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_set_manager
      deployed
      ("tz1fakefakefakefakefakefakefakcphLA5" : address) ;
    let storage = Test.get_storage dex_addr in
    if storage.manager = ("tz1fakefakefakefakefakefakefakcphLA5" : address)
    then ()
    else failwith "incorrect manager"
  end

(* set_lqt_address was exercised during setup and is (will be) tested
   elsewhere *)

let test_default () =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    Test.transfer_to_contract_exn
      (Test.to_contract dex_addr)
      ()
      1tz ;
    ()
  end

let test_update_token_pool () =
  let deployed = basic_setup () in
  let { dex_addr ; tok_addr } = deployed in
  begin
    Test.transfer_to_contract_exn
      (Test.to_entrypoint "mintOrBurn" tok_addr : mintOrBurn contract)
      { quantity = -1 ;
        target = Tezos.address (Test.to_contract dex_addr) }
      0tz ;
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 1000000n ;
        lqt_total = 1000000n } ;
    assert_update_token_pool deployed ;
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 999999n ;
        lqt_total = 1000000n }
  end

(* TODO updateTokenPoolInternal should fail *)

let test_token_to_token () =
  let deployed = basic_setup () in
  let {dex_addr} = deployed in
  begin
    assert_token_to_token
      deployed
      (* TODO deploy another dexter to test *)
      { outputDexterContract = Tezos.address (Test.to_contract dex_addr) ;
        minTokensBought = 497997n ;
        to_ = src () ;
        tokensSold = 500000n ;
        deadline = future } ;
    assert_dex_pools
      deployed
      { xtz_pool = 1tz ;
        token_pool = 1002003n ;
        lqt_total = 1000000n } ;
    assert_tok deployed (src ()) 999999997997n ;
    assert_lqt deployed (src ()) 1000000n
  end

let test () =
  begin
    test_setup ();
    test_add_liquidity ();
    test_add_more_liquidity ();
    test_remove_liquidity ();
    test_xtz_to_token ();
    test_token_to_xtz ();
    test_set_baker ();
    test_set_manager ();
    test_default ();
    test_update_token_pool ();
    test_token_to_token ();
    ()
  end
